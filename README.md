## 心路历程

2018-6-5

ESLint 真是垃圾, 直接关了省心

Stylus 用起来不舒服啊, 还是要用 SCSS

REM 真麻烦, 还是用 VW 吧

### Header

Header 的高度, input 的高度 用了固定的 px

header-left 的宽度, header-right 的宽度都用了 px 固定宽度, 中间 input 的宽度用 flex 撑满

### Swiper

zz 代码

```
.wrapper {
  height: 100px;
  .swiper-img {
    width: 100%;
  }
}
```

正常代码

```
.wrapper {
  height: 26.7vw;
  background-color: #eee;
  .swiper-img {
    width: 100%;
  }
}
```

兼容

```
.wrapper {
  height: 0;
  padding-bottom: 26.7%;
  background-color: #eee;
  .swiper-img {
    width: 100%;
  }
}
```


## 依赖的库

fastclick: 解决移动端 300ms 延迟问题

axios: http 请求

vue-awesome-swiper: 轮播组件

better-scroll: 类似移动端的滚动
